#!/usr/bin/php
<?php
    $stdin = fopen("php://stdin", "r");
    while ($stdin && !feof($stdin)) {
        echo "Entrez un nombre: ";
        $data = fgets($stdin);
        if ($data) {
            $data = str_replace("\n", "", "$data");
            if (is_numeric($data)) {
                if ($data % 2 == 0)
                    echo "Le chiffre " . $data . " est Pair\n";
                else
                    echo "Le chiffre " . $data . " est Impair\n";
            } else
                echo "'" . $data . "' n'est pas un chiffre\n";
        }
    }
    fclose($stdin);
    echo "\n";
?>